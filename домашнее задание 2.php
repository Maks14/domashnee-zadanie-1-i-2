<?php 

// исходный массив
$continents = [
'Africa' => ['Mammuthus columbi', 'Gorilla', 'Parrot jaco', 'Chameleon', 'African elephant', 'Dromedar camel'],
'Eurasia' => ['Noble deer', 'Marsh turtle', 'Calan', 'White hare', 'Crocodile'],
'Antarctica' => ['Sea leopard', 'Seyval', 'Crabeater seal',  'Penguins', 'Blue whale'],
'South America' => ['Goatzin', 'Hummingbird', 'Manatee', 'Harpy', 'Andean condor'],
'Australia' => ['Wombat', 'Couscous', 'Pink сockatoo', 'Bowerbird']
];

print_r($continents);

// 1.Получаем массив из названий животных с двумя словами
$str_two = [];

foreach ($continents as $key => $value) {
if(is_array($value)) {
  foreach($value as $value1) {
  $value1 = trim($value1);
  $value1 = preg_replace('| +|', ' ', $value1);
  if(substr_count($value1, ' ', 0) === 1) {
  array_push($str_two, $value1);
  }
}
}
}

// Выводим результат
echo "<p><b>Названия животных из двух слов:</b></p>" ;

for($i=0; $i < count($str_two); $i++) {
  echo $str_two[$i];
  echo "<br>";  
}


// 2.Разбиваем по отдельным массивам
$massive1 = [];
$massive2 = [];

for($i=0; $i < count($str_two); $i++) {
  $result = explode(' ', $str_two[$i]);
  array_push($massive1, $result[0]);
  array_push($massive2, $result[1]);
}

shuffle($massive1);
shuffle($massive2);


// 3.Получаем массив с фантазийными названиями животных
$massive_fantasy = [];

for($y=0; $y < count($massive1); $y++) {
  $result = $massive1[$y] . ' ' . $massive2[$y];
  array_push($massive_fantasy, $result);
}

// 4. Выводим результат
echo "<p><b>Названия фантазийных животных:</b></p>" ;

for($i=0; $i < count($massive_fantasy); $i++) {
  echo $massive_fantasy[$i];
  echo "<br>";  
}

?>


<?php 
// Дополнительное задание
foreach ($continents as $key => $value) {
    echo "<h2>$key</h2>";
    foreach ($massive_fantasy as $valueF) {
      foreach($value as $value2) {
        if(strtok($value2, ' ') === strtok($valueF, ' '))
        echo $valueF . ', ';
      }
    }
    
}

?>
